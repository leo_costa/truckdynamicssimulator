function dz = modeloCaminhao(t,Z, C, rho, A, m, vdes, Pistas, X0)
%modeloCaminhao Modelo dinamico do caminhao

g = 9.81; % Aceleracao da gravidade
% T - Tracao do caminhao
% K - 1/2 C rho A
% m - massa do caminhao
% theta - inclinação da pista
K = 0.5*C*rho*A;

T = modeloPiloto(K, vdes, Z(2), t, Z(1), X0);

% Recebe o valor de inclinacão para a pista do cenário atual
theta = Pistas.pistaCenarioAtual(t,Z(1));

% Checa se o valor de theta é válido
if isnan(theta)
    theta = 0;
end

% P = T*v
P = T*Z(2);

% Não regenera energia
if P < 0
    P = 0;
end

dz = [Z(2);
      (T - K*Z(2)^2 - m*g*sin(theta))/m;
      P];

end

