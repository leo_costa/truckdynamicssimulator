function plotarGraficos(z,t,Toperador,nomeCenario,createFig,legenda)
%plotarGraficos Plota os gráficos dos estados fornecidos + Toperador
holdFigure = false;
if createFig == true
    figure('Name',nomeCenario,'NumberTitle','off');
    set(gcf,'position',[10,10,2000,800]);
    
else
    holdFigure = true;
end

if holdFigure == true
    hold on;
end
subplot(2,2,1); hold on;
plot(t,z(:,3));
grid on; box on;
title('Energia x Tempo');
xlabel('Tempo [s]', 'Interpreter', 'Latex','FontSize',15);
ylabel('Energia [J]', 'Interpreter', 'Latex','FontSize',15);

if holdFigure == true
    hold on;
end

subplot(2,2,2); hold on;
plot(t,Toperador);
grid on; box on;
title('Toperador  x Tempo');
xlabel('Tempo [s]', 'Interpreter', 'Latex','FontSize',15);
ylabel('Toperador [N]', 'Interpreter', 'Latex','FontSize',15);
leg = legend(legenda);
set(leg,'location','bestoutside')

if holdFigure == true
    hold on;
end

subplot(2,2,3); hold on;
plot(t,z(:,1)); % X
grid on; box on;
title('Posição do caminhão x Tempo');
xlabel('Tempo [s]', 'Interpreter', 'Latex','FontSize',15);
ylabel('Posicao longitudinal [m]', 'Interpreter', 'Latex','FontSize',15);

if holdFigure == true
    hold on;
end

subplot(2,2,4); hold on;
plot(t,3.6*z(:,2)); % V
grid on; box on;
title('Velocidade  x Tempo');
xlabel('Tempo [s]', 'Interpreter', 'Latex','FontSize',15);
ylabel('Velocidade $[\frac{km}{h}]$', 'Interpreter', 'Latex','FontSize',15);

end

