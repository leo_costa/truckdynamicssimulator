function T = modeloPiloto(K, vdes, v, t, x, X0)
%modeloPiloto Modelo do piloto que controla o caminhao
Top = K*vdes^2; % Torque operacional
Kp = 5000;      % Ganho proporcional
Ki = Kp/10;     % Ganho integral
Tmax = 110e3/v; % Tracao maxima do caminhao 
  
T = Kp*(vdes - v) + Ki*(vdes*t - (x-X0)) +  Top;

% Limitacão da tracão máxima
if abs(T) > Tmax
    T = sign(T)*Tmax; 
end

end

