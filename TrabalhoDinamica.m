%% Trabalho de Cinemática e Dinâmica para Automação e Controle
% * Leonardo da Silva Costa
% * João Pedro Correa Argentin
% * Kelven Cleiton Araújo Brandão
% * Vinícius Lemos da Silva

%%
% *Sprinter Chassi - Modelo 2015*
% 
% <</home/leonardo/Din/Sprinter.png>>
% 

%% Inicialização

clear all; close all; clc;
load stretchDataRJPA.mat;

%% Modelo do caminhão
% <</home/leonardo/Din/imagemModeloCaminhao.svg>>
%
% T é a força de tração, D é a força de resistência aerodinâmica, G é a força peso e N é a força normal.
%
% a = d^2x/dt^2, v = dx/dt e x é a posição longitudinal do caminhão. 
%
% As constantes m, C, ρ, A e g são massa total, coeficiente de resistência aerodinâmica, 
%
% densidade do ar, projeção da área frontal e aceleração da gravidade
%
% <</home/leonardo/Din/ModeloCaminhao.svg>>

%% Modelo em espaco de estados do caminhão.
%
% <include>modeloCaminhao.m</include>
%

%% Modelo de consumo
% <</home/leonardo/Din/ModeloEnergia.svg>>

%% Modelo de piloto
% <</home/leonardo/Din/ModeloPiloto.svg>>

%% Modelo de controle de tracão do piloto.
%
% <include>modeloPiloto.m</include>
%

%% Classe utilizada para gerenciar o uso das pistas na simulacao.
%
% <include>Pista.m</include>
%

%% Definição dos cenários e variáveis
tempoMaxSimulacao = 1000; % Tempo máximo de simulacão
Pistas = Pista;
Pistas.Cenario   = 1 ; % Cenário Inicial
Pistas.Angulo    = 1 ; % 1  Grau
Pistas.Amplitude = 20; % 20 Metros
Pistas.AnguloMod = 1 ; % 1  Grau
Pistas.tMax      = tempoMaxSimulacao;
Pistas.dadosCenario4 = struct('slope',stretchDataRJPA.slope(111:190), 'dist',stretchDataRJPA.dist(111:190));
                              % Indices 110 - 190 : Primeiro trecho verde
                              
vMin = 40 ; % Velocidade mínima do caminhão no cenário 4 [km/h]
vMax = 120; % Velocidade máxima do caminhão no cenário 4 [km/h]
                              
anguloMax    = 20  ; % Angulo    máximo de simulação para o cenário 1
amplitudeMax = 115 ; % Amplitude máxima de simulação para o cenário 2
anguloModMax = 15  ; % Angulo    máximo de simulação para o cenário 3

% Inicializacão dos vetores para armazenar a energia consumida 
energiaAnguloCenario1     = zeros(anguloMax                                  , 2);
energiaAmplitudeCenario2  = zeros(length(Pistas.Amplitude : 5 : amplitudeMax), 2);
energiaAnguloCenario3     = zeros(anguloModMax                               , 2);
energiaVelocidadeCenario4 = zeros(length(vMin:5:vMax)                        , 2);
indiceAux = 1;

% Parâmetros dos gráficos
set(0,'DefaultAxesColorOrder',jet(20)); % Muda a cor
%% Constantes
C = 0.44065; % Coeficiente de resistencia aerodinamica [kg/m3]
rho = 1.22;  % Densidade do ar [km/m3]
A = 6.73615; % Area projetada [m2]
m = 7000;    % massa do caminhao [kg]

%% Parametros de Integracao
tspan = 0 : 1 : tempoMaxSimulacao;% Simula por tempoMaxSimulacao segundos
Vdesejada = 60 / 3.6; % 60 km/h para os cenários 1, 2 e 3
Z0 = [0 Vdesejada 0]; % Estado inicial
% z = [Posicao Velocidade Energia]

%% Simulacão Cenario 1 - Parâmetro de variação - Ângulo da pista [1~20] [graus]
Pistas.Cenario = 1;
criaGrafico = true;

for angulo = Pistas.Angulo : anguloMax
    [tCenario1,zCenario1] = ode45(@(t,z)modeloCaminhao(t, z, C, rho, A, m, Vdesejada, Pistas, Z0(1)), tspan, Z0);
    
    % Calcula a tração do operador nos trajetos
    Toperador1 = calculaTracao(tCenario1, zCenario1, C, rho, A, Vdesejada, Z0(1));
    
    % Atualiza a legenda do gráfico
    legendaCenario1{indiceAux} = "Ângulo = " + angulo;
    
    % Plota os gráficos para os cenários
    plotarGraficos(zCenario1,tCenario1,Toperador1,"Cenario 1 - Angulo de inclinação = "+Pistas.Angulo+"~"+ anguloMax,criaGrafico,legendaCenario1);
    
    if criaGrafico == true
        criaGrafico = false;
    end
    
    Pistas.Angulo = angulo;
    
    energiaAnguloCenario1(indiceAux,:) = [zCenario1(end,3), angulo];
    indiceAux = indiceAux+1;
end

plotarEnergia(energiaAnguloCenario1, "Cenário 1", 'Energia x Angulo', 'Angulo [graus]');

%% Simulacão Cenario 2 - Parâmetro de variacão - Amplitude da pista [20~115] [m]
Pistas.Cenario = 2;
indiceAux = 1;
criaGrafico = true;

for amplitude = Pistas.Amplitude : 5 : amplitudeMax
    [tCenario2,zCenario2] = ode45(@(t,z)modeloCaminhao(t, z, C, rho, A, m, Vdesejada, Pistas, Z0(1)), tspan, Z0);
    
    % Calcula a tração do operador nos trajetos
    Toperador2 = calculaTracao(tCenario2, zCenario2, C, rho, A, Vdesejada, Z0(1));

    % Atualiza a legenda do gráfico
    legendaCenario2{indiceAux} = "Amplitude = " + amplitude;
                                     
    % Plota os gráficos para os cenários
    plotarGraficos(zCenario2,tCenario2,Toperador2, "Cenario 2 - Amplitude de oscilação = "+Pistas.Amplitude+"~"+amplitudeMax+" [m]",criaGrafico,legendaCenario2);
    
    
    if criaGrafico == true
        criaGrafico = false;
    end
    
    Pistas.Amplitude = amplitude;
    
    energiaAmplitudeCenario2(indiceAux,:) = [zCenario2(end,3), amplitude];
    indiceAux = indiceAux+1;
end

plotarEnergia(energiaAmplitudeCenario2, "Cenário 2", 'Energia x Amplitude', 'Amplitude [m]');

%% Simulacão Cenario 3 - Parâmetro de variação - Ângulo da pista [1~15] [graus]
Pistas.Cenario = 3;
indiceAux = 1;
criaGrafico = true;
tspan = 0 : 1 : 95;% Simula por 95 segundos


for anguloMod = Pistas.AnguloMod : anguloModMax
    [tCenario3,zCenario3] = ode45(@(t,z)modeloCaminhao(t, z, C, rho, A, m, Vdesejada, Pistas, Z0(1)),tspan, Z0);
                                     
    % Calcula a tração do operador nos trajetos
    Toperador3 = calculaTracao(tCenario3, zCenario3, C, rho, A, Vdesejada, Z0(1));

    % Atualiza a legenda do gráfico
    legendaCenario3{indiceAux} = "Ângulo = |" + anguloMod + "|";
    
    % Plota os gráficos para os cenários
    plotarGraficos(zCenario3,tCenario3,Toperador3,"Cenario 3 - Angulo de inclinação = |"+Pistas.AnguloMod+"~"+anguloModMax+"|", criaGrafico,legendaCenario3);

    if criaGrafico == true
        criaGrafico = false;
    end
    
    Pistas.AnguloMod = anguloMod;
    
    energiaAnguloCenario3(indiceAux,:) = [zCenario3(end,3), anguloMod];
    indiceAux = indiceAux+1;
end

plotarEnergia(energiaAnguloCenario3, "Cenário 3", 'Energia x Angulo', 'Angulo [graus]');

%% Simulacão Cenario 4 - Parâmetro de variação - Velocidade do caminhão [40~120] [km/h]
Pistas.Cenario = 4;
indiceAux = 1;
criaGrafico = true;
X0 = Pistas.dadosCenario4.dist(1);


for velCaminhao = vMin : 5 : vMax
    
    tspan = 0 : 1 : 158e3/(velCaminhao/3.6);% Simula por X segundos
    Z0 = [X0 velCaminhao/3.6 0]; % Estado inicial


    [tCenario4,zCenario4] = ode45(@(t,z)modeloCaminhao(t, z, C, rho, A, m, velCaminhao/3.6, Pistas, Z0(1)), tspan, Z0);
                                     
    % Calcula a tração do operador nos trajetos
    Toperador4 = calculaTracao(tCenario4, zCenario4, C, rho, A, velCaminhao/3.6,Z0(1));

    % Atualiza a legenda do gráfico
    legendaCenario4{indiceAux} = "Velocidade = " + velCaminhao + " [km/h]";
    
    % Plota os gráficos para os cenários
    plotarGraficos(zCenario4,tCenario4,Toperador4,"Cenario 4 - Velocidade do caminhão = "+vMin+"~"+vMax+" km/h",criaGrafico,legendaCenario4);

    if criaGrafico == true
        criaGrafico = false;
    end
        
    energiaVelocidadeCenario4(indiceAux,:) = [zCenario4(end,3), velCaminhao];
    indiceAux = indiceAux+1;
end

plotarEnergia(energiaVelocidadeCenario4, "Cenário 4", 'Energia x Velocidade', 'Velocidade [km/h]');

%% Funcoes auxiliares
% Funcão auxilliar para plotar os gráficos.
%
% <include>plotarEnergia.m</include>
%
% <include>plotarGraficos.m</include>
%