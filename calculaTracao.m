function Toperador = calculaTracao(t, Z, C, rho, A, vdes, X0)
%calculaTracao Calcula a tracao do operador
K = 0.5*C*rho*A;

Toperador = zeros(length(Z(:,1)),1);

if length(Z(:,1)) > 1
    for n=1:length(Z)
        Toperador(n) = modeloPiloto(K, vdes, Z(n,2), t(n), Z(n,1), X0);
    end
end

end

