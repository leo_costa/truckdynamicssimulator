classdef Pista
    %Pista Classe que contem os perfis de pista utilizados
    properties
        Cenario   % Define o cenário que esta pista representa
        Angulo    % Angulo de inclinação da pista do cenário 1
        Amplitude % Amplitude da senoide utilizada na pista do cenário 2
        AnguloMod % Modulo do angulo da pista do cenário 3
        tMax      % Tempo máximo de simulação
        dadosCenario4 % Dados do cenario 4
    end
    
    methods
        function theta = inclinacaoCenario1(obj,~)
            %inclinacaoCenario1 Retorna a inclinacao da pista 1 [rad]
            theta = obj.Angulo * pi/180;
        end
        
        function theta = inclinacaoCenario2(obj,xCam)
            %inclinacaoCenario2 Retorna a inclinacao da pista 2 [rad]
            theta = atan(obj.Amplitude*cos(xCam*1e-3)*1e-3);
        end
        
        function theta = inclinacaoCenario3(obj,~,xCam)
            %inclinacaoCenario3 Retorna a inclinacao da pista 3 [rad]
            theta = obj.AnguloMod*pi/180*sign((-1)^(floor(xCam/200)));
        end
        
        function theta = inclinacaoCenario4(obj,xCam)
            %inclinacaoCenario4 Retorna a inclinacao da pista 4 [rad]
            theta = interp1(obj.dadosCenario4.dist,obj.dadosCenario4.slope, xCam);
        end
        
        function theta = pistaCenarioAtual(obj,t,xCam)
            %pistaCenarioAtual Retorna a inclinação para a pista do cenario atual
            switch obj.Cenario
                case 1
                    theta = obj.inclinacaoCenario1(t);
                case 2
                    theta = obj.inclinacaoCenario2(xCam);
                case 3
                    theta = obj.inclinacaoCenario3(t,xCam);
                case 4
                    theta = obj.inclinacaoCenario4(xCam);
                otherwise
                    theta = 0;
            end
        end
    end
end

