function plotarEnergia(energiaParametro,tituloJanela,tituloGraf,tituloX)
%plotEnergia Plota os graficos de energia

if tituloJanela ~= "Cenário 4"
    figure('Name', tituloJanela,'NumberTitle','off');
    set(gcf,'position',[10,10,800,600]);
    plot(energiaParametro(:,2),energiaParametro(:,1));
    grid on; box on;
    title(tituloGraf);
    xlabel(tituloX, 'Interpreter', 'Latex','FontSize',15);
    ylabel('Energia [J]', 'Interpreter', 'Latex','FontSize',15);
else
    figure('Name', tituloJanela,'NumberTitle','off');
    set(gcf,'position',[10,10,800,600]);
    
    subplot(1,2,1);
    plot(energiaParametro(:,2),energiaParametro(:,1));
    grid on; box on;
    title(tituloGraf);
    xlabel(tituloX, 'Interpreter', 'Latex','FontSize',15);
    ylabel('Energia [J]', 'Interpreter', 'Latex','FontSize',15);
    
    % Densidade volumétrica do Diesel = 0.85 kg/L  
    % Poder calorífico inferior do Diesel (PCI) = 43e6 J/kg
    % PCI Diesel [J/L] * 30% (Rendimento)
    pciL = 43e6*0.85*0.3;
    consumoLitros = 158./(energiaParametro(:,1)/pciL);
    
    subplot(1,2,2);
    plot(energiaParametro(:,2),consumoLitros);
    grid on; box on;
    title("Consumo x Velocidade");
    xlabel(tituloX, 'Interpreter', 'Latex','FontSize',15);
    ylabel('Consumo de Diesel [km/L]', 'Interpreter', 'Latex','FontSize',15);
end

end

